﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChallengeAmericaVirtual.Models
{
    public class Image
    {
        protected string url;

        public string Url
        {
            get { return this.url; }
            set { this.url = value; }
        }


        protected string thumbnail;
        public string Thumbnail
        {
            get { return thumbnail ?? "false"; }
            set { thumbnail = value; }
        }


        static public Image[] Get(System.Xml.Linq.XElement data)
        {
            if (data != null)
            {

                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(data.CreateReader());
                if (doc.DocumentElement.ChildNodes.Count > 0)
                {
                    Image[] rt = new Image[doc.DocumentElement.ChildNodes.Count];
                    int i = 0;
                    foreach (System.Xml.XmlNode n in doc.DocumentElement.ChildNodes)
                    {
                        rt[i] = new Image() { Url = n.InnerText };
                        if (n.Attributes["thumbnail"] != null)
                            rt[i].Thumbnail = n.Attributes["thumbnail"].Value;
                        i++;
                    }
                    return rt;

                }

            }
            return new Image[0];
        }

      
    }
}