﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChallengeAmericaVirtual.Models
{
    public class LogInResult
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
    }


        public class LoginModel
        {
            [Required]
            [Display(Name = "Nombre de usuario")]
            public string UserName { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Contraseña")]
            public string Password { get; set; }

            [Display(Name = "¿Recordar cuenta?")]
            public bool RememberMe { get; set; }

            public string ReturnUrl { get; set; }
        }
    
}