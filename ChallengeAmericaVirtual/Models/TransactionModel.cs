﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChallengeAmericaVirtual.Models
{
    public class TransactionModel
    {
        public TransactionLog Transaction{ get; set; }
        public Items Item { get; set; }
        public Users User { get; set; }

    }
}