﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChallengeAmericaVirtual
{

    [Flags]
    public enum ItemTypes
    {
        None = 0,
        Category = 1,
        Product = 2,
        ProductOption = 4
    }

    [Flags]
    public enum TransactionType
    {
        Login = 0,
        Logout = 1,
        Sale = 2,
        FailSale = 4
    }

    public class Helpers
    {

        static public string base64Encode(string data)
        {
            try
            {
                byte[] encData_byte = new byte[data.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
                string encodedData = System.Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (System.Exception e)
            {
                throw new System.Exception("Error in base64Encode" + e.Message);
            }
        }


        static public string EncryptPassword(string password)
        {

            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
            data = x.ComputeHash(data);
            return base64Encode(System.Text.Encoding.ASCII.GetString(data));
        }




    }
}