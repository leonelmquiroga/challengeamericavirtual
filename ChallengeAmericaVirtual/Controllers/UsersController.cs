﻿using ChallengeAmericaVirtual.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ChallengeAmericaVirtual.Controllers
{
    public class UsersController : BaseController
    {
        // GET: Users
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult LoginBox()
        {
            return PartialView();
        }


        [HttpPost]
        public ActionResult DoLogin(LoginModel model, string ReturnUrl)
        {
            if (this.Request.IsAuthenticated)
                System.Web.Security.FormsAuthentication.SignOut();

            Users l = new Users();
            try
            {
                l = LogIn(model.UserName, model.Password);
            }
            catch (System.Exception ex)
            {
                if(!String.IsNullOrEmpty(ReturnUrl))
                    return View(ReturnUrl);
                return View(l);
            }


            this.CurrentUser = l;

            FormsAuthentication.SetAuthCookie(model.UserName, false);
            string roles = "client";

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
              1,
              l.UserName,  //user id    
              DateTime.Now,
              DateTime.Now.AddDays(1),  // expiry
              model.RememberMe,  //do not remember
              roles,
              "/");
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName,
                                               FormsAuthentication.Encrypt(authTicket));
            Response.Cookies.Add(cookie);


            Models.TransactionLog transaction = new Models.TransactionLog();
            transaction.UserId = this.CurrentUser.id;
            transaction.Description = "New Login from user " + this.CurrentUser.UserName;         
            transaction.Type = (int)TransactionType.Login;
            transaction.Date = DateTime.Now;
            try
            {
                Db.TransactionLog.InsertOnSubmit(transaction);
                Db.SubmitChanges();
            }
            catch(DbException ex){}

            if (!String.IsNullOrEmpty(ReturnUrl))
                return Redirect(ReturnUrl);
            return View(l);
        }

        private Users LogIn(string username, string password)
        {
            Users c = new Users();
            string Enc_password = Helpers.EncryptPassword(password);
            try
            {
                c = (from u in Db.Users where u.UserName == username && u.Password == Enc_password select u).FirstOrDefault();
            }
            catch
            {

            }
            return c;
        }

        public ActionResult LogOff(string ReturnUrl)
        {
            var loggedOutUser = this.CurrentUser;
            this.CurrentUser = null;
            Session["VendorUser"] = null;
            FormsAuthentication.SignOut();

            Models.TransactionLog transaction = new Models.TransactionLog();
            transaction.UserId = loggedOutUser.id;
            transaction.Description = "New Logout from user " + loggedOutUser.UserName;
            transaction.Type = (int)TransactionType.Logout;
            transaction.Date = DateTime.Now;
            try
            {
                Db.TransactionLog.InsertOnSubmit(transaction);
                Db.SubmitChanges();
            }
            catch (DbException ex) { }

            if (!String.IsNullOrEmpty(ReturnUrl))
            {
                return Redirect(ReturnUrl);
            }

            return RedirectToAction("Index", "Home");

        }


    }
}