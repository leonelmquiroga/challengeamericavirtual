﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChallengeAmericaVirtual.Controllers
{
    public class ItemsController : BaseController
    {

        

        // GET: Items
        public ActionResult Index(string id)
        {
            if (string.IsNullOrEmpty(id))
                return HttpNotFound();
            string nuid = id != null && id.EndsWith("/") ? id.Remove(id.Length - 1) : id;



            Models.Items i = null;

            i = ReadByPath(nuid);

            string viewname = "";

            switch ((ItemTypes)i.ItemType)
            {
                case ItemTypes.Category: viewname = "Category/Detail"; break;
                case ItemTypes.Product: viewname = "Product/Detail"; break;

            }
            if(!String.IsNullOrEmpty(viewname))
                return View(viewname,i);
            return View(i);

        }

        private Models.Items ReadByPath(string path)
        {

            var c = (from itemc in Db.Items where (itemc.ItemType == 1 || itemc.ItemType == 2) && itemc.Path.Contains(path) select itemc).Take(1);
            if (c.Count() < 1)
                throw new Exception("Item not found");
            Models.Items ct = c.Single();

            return ct;
        }


        public Models.Items GetProduct(int id)
        {
            var c = (from itemc in Db.Items where (itemc.ItemType == 2 || itemc.ItemType == 4) && itemc.Id == id select itemc).Take(1);
            if (c.Count() < 1)
                throw new Exception("Item not found");
            Models.Items ct = c.Single();

            return ct;
        }

        public ActionResult GetOptions(int ParentId , string view)
        {

            List<Models.Items> items = new List<Models.Items>();

            try
            {
                var c = (from itemc in Db.Items where itemc.ItemType == (int)ItemTypes.ProductOption && itemc.ParentId == ParentId select itemc).ToList();
                if (c.Count() < 1)
                    throw new Exception("Items not found");
                items = c;
            }
            catch { }
            if(!String.IsNullOrEmpty(view))
                return PartialView(view, items);
            return PartialView(items);

        }

        public ActionResult Images(System.Xml.Linq.XElement dataImg)
        {
            
            return PartialView(Models.Image.Get(dataImg));
        }



        public ActionResult Checkout(int itemId, int qty)
        {
            Models.TransactionLog transaction = new Models.TransactionLog();
            Models.TransactionModel ShowTransaction = new Models.TransactionModel();
            if(this.Request.IsAuthenticated)
            {
                transaction.UserId = this.CurrentUser.id ;
                ShowTransaction.User = this.CurrentUser;
            }

            transaction.Description = "New sale from " + (this.Request.IsAuthenticated ? "user " + this.CurrentUser.UserName : "Guest");
            transaction.Type = (int)TransactionType.Sale;
            transaction.Date = DateTime.Now;

            Models.Items item = null;
                
            try
            {
                item = GetProduct(itemId);
            }
            catch{ }

            if(item == null)
            {
                transaction.Description = "FAIL SALE";
                transaction.Type = (int)TransactionType.FailSale;
                return Json(transaction, JsonRequestBehavior.AllowGet);
            }

            transaction.ItemId = itemId;
            transaction.Quantity = qty;
            transaction.ItemPrice = item.OfferPrice;
            ShowTransaction.Item = item;
            ShowTransaction.Transaction = transaction;
            try
            {
                Db.TransactionLog.InsertOnSubmit(transaction);

                Db.SubmitChanges();
            }
            catch(DbException ex) {
                transaction.Description  += ex.Message;
            }

   
            
            return Json(ShowTransaction, JsonRequestBehavior.AllowGet);
        }


    }
}