﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChallengeAmericaVirtual.Controllers.API
{
    public class ProductsController : ApiBaseController
    {
        // GET: api/Products
        public IEnumerable<Models.Items> Get()
        {

            var c = (from itemc in Db.Items where (itemc.ItemType == 2 || itemc.ItemType == 4)  select itemc).ToArray();
            if (c.Count() < 1)
                throw new Exception("Item not found");
            IEnumerable<Models.Items> ct = c;

            return ct;
        }

        // GET: api/Products/5
        public Models.Items Get(int id)
        {
            var c = (from itemc in Db.Items where (itemc.ItemType == 1 || itemc.ItemType == 2) && itemc.Id == id select itemc).Take(1);
            if (c.Count() < 1)
                throw new Exception("Item not found");
            Models.Items ct = c.Single();

            return ct;
        }

        // POST: api/Products
        public Models.Items Post([FromBody]Models.Items item)
        {
            Models.Items newItem = item;

            try
            {
                Db.Items.InsertOnSubmit(item);
                Db.SubmitChanges();
            }
            catch(DbException ex) {
                throw ex;
            }

            return item;

        }

        // PUT: api/Products/5
        public Models.Items Put([FromBody]Models.Items item)
        {
            try
            {

                var modItem = (from itemM in Db.Items where itemM.Id == item.Id select itemM).SingleOrDefault();

                if (modItem.ParentId != item.ParentId)
                    modItem.ParentId = item.ParentId;
                if (modItem.ItemType != item.ItemType)
                    modItem.ItemType = item.ItemType;
                if (modItem.Name != item.Name)
                    modItem.Name = item.Name;
                if (modItem.ShortDescription != item.ShortDescription)
                    modItem.ShortDescription = item.ShortDescription;
                if (modItem.Description != item.Description)
                    modItem.Description = item.Description;
                if (modItem.Code != item.Code)
                    modItem.Code = item.Code;
                if (modItem.OfferPrice != item.OfferPrice)
                    modItem.OfferPrice = item.OfferPrice;
                if (modItem.DummyPrice != item.DummyPrice)
                    modItem.DummyPrice = item.DummyPrice;
                if (modItem.Published != item.Published)
                    modItem.Published = item.Published;
                if (modItem.ForSale != item.ForSale)
                    modItem.ForSale = item.ForSale;
                if (modItem.Stock != item.Stock)
                    modItem.Stock = item.Stock;
                if (modItem.Path != item.Path)
                    modItem.Path = item.Path;


                Db.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
            catch (DbException ex)
            {
                throw ex;
            }

            return item;
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
            try
            {
                var modItem = (from itemM in Db.Items where itemM.Id == id select itemM).FirstOrDefault();

                Db.Items.DeleteOnSubmit(modItem);

                Db.SubmitChanges();
            }
            catch (DbException ex)
            {
                throw ex;
            }
        }
    }
}
