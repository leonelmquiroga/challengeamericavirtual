﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChallengeAmericaVirtual.Controllers.API
{
    public class TransactionsController : ApiBaseController
    {
        // GET: api/Transactions
        public IEnumerable<Models.TransactionModel> Get()
        {
            var c = (from trasc in Db.TransactionLog select trasc).ToArray();
            if (c.Count() < 1)
                throw new Exception("Item not found");

            List<Models.TransactionModel> transcModelList = new List<Models.TransactionModel>();

            foreach(var transc in c)
            {
                Models.TransactionModel transcModel = new Models.TransactionModel();

                if (transc.UserId != null)
                {
                    try
                    {
                        transcModel.User = (from us in Db.Users where us.id == transc.UserId select us).FirstOrDefault();
                    }
                    catch { }
                }

                if(transc.ItemId != null)
                {
                    var it = (from itemc in Db.Items where (itemc.ItemType == 2 || itemc.ItemType == 4) && itemc.Id == transc.ItemId select itemc).Take(1);
                    if (it.Count() < 1)
                        throw new Exception("Item not found");
                    Models.Items its = it.Single();

                    transcModel.Item = its;
                }

                transcModelList.Add(transcModel);
            }

            return transcModelList;
        }

        // GET: api/Transactions/5
        public Models.TransactionModel Get(int id)
        {
            var c = (from trasc in Db.TransactionLog where trasc.Id == id select trasc).Take(1);
            if (c.Count() < 1)
                throw new Exception("Item not found");
            Models.TransactionLog ct = c.Single();

            Models.TransactionModel transcModel = new Models.TransactionModel();
            if (ct.UserId != null)
            {
                try
                {
                    transcModel.User = (from us in Db.Users where us.id == ct.UserId select us).FirstOrDefault();
                }
                catch { }
            }

            if (ct.ItemId != null)
            {
                var it = (from itemc in Db.Items where (itemc.ItemType == 2 || itemc.ItemType == 4) && itemc.Id == ct.ItemId select itemc).Take(1);
                if (it.Count() < 1)
                    throw new Exception("Item not found");
                Models.Items its = it.Single();

                transcModel.Item = its;
            }

            return transcModel;
        }


    }
}
