﻿using ChallengeAmericaVirtual.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChallengeAmericaVirtual.Controllers.API
{
    public class ApiBaseController : ApiController
    {
        protected ChallengeDataContext _db;

        public ChallengeDataContext Db
        {
            get
            {
                if (this._db == null)
                {

                    if ( StackExchange.Profiling.MiniProfiler.Current != null)
                    {
                        var conn = new StackExchange.Profiling.Data.ProfiledDbConnection(new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ChallengeAmericaVirtualConnectionString"].ConnectionString), StackExchange.Profiling.MiniProfiler.Current);

                        this._db = new ChallengeDataContext(conn);
                    }
                    else
                        this._db = new ChallengeDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["ChallengeAmericaVirtualConnectionString"].ConnectionString);
                }
                return this._db;
            }
        }

    }
}
