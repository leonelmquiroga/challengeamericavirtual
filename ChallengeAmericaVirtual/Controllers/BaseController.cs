﻿using ChallengeAmericaVirtual.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChallengeAmericaVirtual.Controllers
{
    public class BaseController : Controller
    {
        protected ChallengeDataContext _db;

        public ChallengeDataContext Db
        {
            get
            {
                if (this._db == null)
                {

                    if (HttpContext.IsDebuggingEnabled && StackExchange.Profiling.MiniProfiler.Current != null)
                    {
                        var conn = new StackExchange.Profiling.Data.ProfiledDbConnection(new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ChallengeAmericaVirtualConnectionString"].ConnectionString), StackExchange.Profiling.MiniProfiler.Current);

                        this._db = new ChallengeDataContext(conn);
                    }
                    else
                        this._db = new ChallengeDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["ChallengeAmericaVirtualConnectionString"].ConnectionString);
                }
                return this._db;
            }
        }


        public Users CurrentUser
        {

            get
            {
                if (Session["_CurrentUser"] != null)
                {
                    ViewBag.CurrentUser = (Users)Session["_CurrentUser"];
                    return (Users)Session["_CurrentUser"];
                }
                else if (this.Request.IsAuthenticated)
                {
                    try
                    {
                        Users u = (from user in Db.Users where user.UserName == this.User.Identity.Name select user).FirstOrDefault();

                        Session["_CurrentUser"] = u;
                        ViewBag.CurrentUser = u;

                    }
                    catch/* que paso????/*/
                    {
                        System.Web.Security.FormsAuthentication.SignOut();
                        return new Users();
                        ViewBag.CurrentUser = null;
                    }
                    return (Users)Session["_CurrentUser"];
                }
                return null;

            }
            set
            {
                if (Session["_CurrentUser"] != null)
                    Session.Remove("_CurrentUser");
                else
                    System.Web.Security.FormsAuthentication.SignOut();
                Session.Add("_CurrentUser", value);

            }

        }



        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            /*Puede ser que esto se ejecute desde Razor -descabezado-, asi que chequeo si hay session...*/
            if (Session == null)
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            if (this.CurrentUser == null || String.IsNullOrEmpty(this.CurrentUser.UserName))
                this.CurrentUser = null;

            ViewBag.CurrentUser = this.CurrentUser;
            base.OnActionExecuting(filterContext);

        }


        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

    }
}