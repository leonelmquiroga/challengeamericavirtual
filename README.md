# ChallengeAmericaVirtual

## Que se realizo?

Se realizó una aplicación MVC con datos básicos de interacción con una base de datos en un contexto de lo que seria una operacion de ecommerce. La aplicación permite hacer login y logout a partir de un usuario creado en la base de datos, guardar los login y logout en un log , visualizar un producto que existe en la base de datos y comprar este producto creando un log con el detalle de la operación. Además de esto hay un API por la cual se pueden ver y modificar productos y un API para ver los eventos que hubieron en el programa (login, logout y compra de producto)

## Que utilice para realizar la aplicación?
Es una aplicación de .NET MVC 4 con una conexión a base de datos hecha por medio de Linq to Sql. La base de datos es una base SQL Server. Para una mejor visualización de la API se instalo Swagger en la aplicación.

## Como ver el sitio?

Bajarse este repositorio y abrir la solución en Visual Studio. La solucion ya tiene todos los paquetes por lo que no tendría que haber problemas para ejecutar el programa. De haber un problema recomendación de borrar la carpeta Obj y BIN y volver a compilar el programa. Para poder usar el sitio en localhost hay que crear una base de datos en SQL Server y montar el backup que se encuentra en la carpeta DBBackup. Luego solo hay que cambiar el connection String por uno que apunte a su localhost. Cuando se inicie la pagina se va a levantar un sitio en la que la home es un formulario de registro. Este registro se ingresa con el usuario leonelmq y la clave es 123456. Una vez logueado te redirige a una pagina donde hay datos básicos de un producto que tiene opciones ( a nivel base de datos las opciones de un producto también son productos pero con un campo llamado ItemType que los diferencia de los productos reales). El producto es accesible mediante un Routing que hacer match de la URL con la URL almacenada en un campo de la base de datos (en un ABM completo, en la creación del producto la url se armaria al crearlo o modificarlo con el nombre de la categoría y el nombre del producto sin caracteres especiales ni espacios).
Este producto puede ser comprado por medio de un botón. Una vez comprado se guarda una venta en una tabla de la base de datos que registra todos los eventos, incluso logins y logouts. Para finalizar el flujo se ve un json que muestra la transacción y el detalle.

En cuanto al API, los dos métodos existentes son <LOCALHOST>/api/Transactions/ y <LOCALHOST>/api/Products. Pueden ver el detalle en <LOCALHOST>/swagger/ui/index#/

Products permite ver un producto, listar todos , borrar, crear o modificar por medio de la APi y Transactions solamente permite ver una operación o todas.

Por tema de tiempo no puede armar todo con un front mas presentable pero espero que la funcionalidad les parezca bien.

